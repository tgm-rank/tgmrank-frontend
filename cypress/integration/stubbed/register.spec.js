describe('Register', () => {
  it('Should show the registration page', () => {
    cy.visit('/');

    // TODO: Add better attributes to tabs and buttons to make the selectors more foolproof
    cy.get('.header [href="/sign-up"]').should('be.visible');
    cy.get('.header [href="/sign-up"]').click();
    cy.validatePath('/sign-up');
    cy.get('.tab--active').contains('Sign up');

    // Switch tabs
    cy.get('[href="/login"]').click();
    cy.get('.tab--active').contains('Login');

    cy.get('[href="/sign-up"]').click();
    cy.validatePath('/sign-up');
    cy.get('.tab--active').contains('Sign up');
  });
});
