import React, { useContext, useState } from 'react';
import Cookie from 'js-cookie';

const ThemeContext = React.createContext();

export function ThemeProvider({ children }) {
  const initialState = () => Cookie.get('dark-theme') === 'true';
  const [darkTheme, setState] = useState(initialState);

  function setDarkTheme(darkTheme) {
    Cookie.set('dark-theme', darkTheme, {
      expires: 365,
      sameSite: 'strict',
      secure: process.env.NODE_ENV === 'production',
    });
    setState(darkTheme);
  }

  return (
    <ThemeContext.Provider value={{ darkTheme, setDarkTheme }}>
      {children}
    </ThemeContext.Provider>
  );
}

export function useTheme() {
  return useContext(ThemeContext);
}

export const ThemeConsumer = ThemeContext.Consumer;
