import React from 'react';

export default function BreadcrumbArrow() {
  return (
    <svg viewBox="0 0 32 32" className="breadcrumb-arrow">
      <path d="M22 16c0 2-6 7-7 8s-4 1-4-2V10c0-3 3-3 4-2s7 6 7 8z" />
    </svg>
  );
}
