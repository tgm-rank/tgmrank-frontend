import React from 'react';
import { Helmet } from 'react-helmet-async';
import { Link } from '@reach/router';
import root from 'window-or-global';
import { t, Trans } from '@lingui/macro';
import { I18n, useLingui } from '@lingui/react';
import LoginForm from '../../components/login-form';
import SignUpForm from '../../components/sign-up-form';
import { ExistingAccountDisclaimer } from '../../components/disclaimer';
import {
  ForgotPasswordForm,
  ResetPasswordForm,
} from '../../components/forgot-password-form';
import { TabsContainer, Tabs, Panels } from '../../components/tabs';
import Logo from '../../../images/logo';

import './styles.css';

function HalfLayout({ children }) {
  return (
    <>
      <header className="header header--simple">
        <div className="header-logo">
          <Link to="/">
            <Logo />
          </Link>
        </div>
      </header>
      <div className="login">
        <div />
        {children}
      </div>
    </>
  );
}

export default function Login() {
  const { i18n } = useLingui();

  const tabs = new Map([
    [t`Login`, '/login'],
    [t`Sign up`, '/sign-up'],
  ]);

  return (
    <HalfLayout>
      <TabsContainer
        selectedTab={root.location?.pathname === '/login' ? 0 : 1}
        labels={Array.from(tabs.keys())}
        render={(context, activeTabLabel) => (
          <div>
            <Helmet>
              <title>{i18n._(activeTabLabel)}</title>
            </Helmet>
            <h1 className="grid-col-6">
              <Trans>Welcome to TGM Rank</Trans>
            </h1>
            <Tabs
              context={context}
              tab={tabName => (
                <Link to={tabs.get(tabName)}>
                  <Trans id={tabName} />
                </Link>
              )}
            />
            <Panels context={context}>
              <LoginForm />
              <SignUpForm />
            </Panels>
            <div className="grid-col-6">
              <ExistingAccountDisclaimer />
            </div>
          </div>
        )}
      />
    </HalfLayout>
  );
}

export function ForgotPasswordPage() {
  return (
    <HalfLayout>
      <div>
        <Helmet>
          <title>Forgot Password</title>
        </Helmet>
        <h1 className="grid-col-6">
          <Trans>Forgot Password</Trans>
        </h1>
        <ForgotPasswordForm />
      </div>
    </HalfLayout>
  );
}

export function ResetPasswordPage(props) {
  return (
    <HalfLayout>
      <div>
        <Helmet>
          <title>Reset Password</title>
        </Helmet>
        <h1 className="grid-col-6">
          <Trans>Reset Password</Trans>
        </h1>
        <ResetPasswordForm {...props} />
      </div>
    </HalfLayout>
  );
}
