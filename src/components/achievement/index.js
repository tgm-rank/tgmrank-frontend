import React, { useState } from 'react';

import useSWR from 'swr';
import { Trans } from '@lingui/macro';
import { useGames } from '../../hooks/use-games';

import TgmRank, { ScoreStatus } from '../../tgmrank-api';

import { Circle, CheckCircle } from 'react-feather';
import LongDate from '../date';
import Link from '../link';

import './styles.css';

function AchievementCheckmark({ isAchieved }) {
  return isAchieved ? <CheckCircle size={16} /> : <Circle size={16} />;
}

export default function PlayerAchievements({ playerId }) {
  const { games } = useGames();
  const [achievementFilter, setAchievementFilter] = useState(null);

  const { data: achievementsList } = useSWR(TgmRank.getAchievementListUrl(), {
    refreshInterval: 30000,
    focusThrottleInterval: 30000,
    dedupingInterval: 30000,
  });

  const { data: playerAchievements } = useSWR(
    playerId != null ? TgmRank.getPlayerAchievementsUrl({ playerId }) : null,
  );

  const playerAchievementsLookup = playerAchievements?.reduce((acc, item) => {
    acc[item.achievementId] = item;
    return acc;
  }, {});

  const sortedAchievementsList = achievementsList
    ?.sort((a, b) => a.sortOrder - b.sortOrder)
    ?.filter(a => {
      const achievementFilterGameId = Number.parseInt(achievementFilter);

      if (Number.isInteger(achievementFilterGameId)) {
        return a.gameId === achievementFilterGameId;
      }

      return true;
    });

  function onSetAchievementFilter(event) {
    setAchievementFilter(event.target?.value);
  }

  return (
    <div>
      <div style={{ display: 'flex', gap: '0.5em' }}>
        <span>
          <input
            type="radio"
            id="all"
            name="gameId"
            value={''}
            checked={
              achievementFilter == null || achievementFilter.length === 0
            }
            onChange={onSetAchievementFilter}
          />
          <label htmlFor="all">
            <Trans>All</Trans>
          </label>
        </span>
        {games?.slice(1)?.map(g => (
          <React.Fragment key={g.gameId}>
            <span>
              <input
                type="radio"
                id={g.shortName}
                name="gameId"
                value={g.gameId}
                checked={achievementFilter === g.gameId.toString()}
                onChange={onSetAchievementFilter}
              />
              <label htmlFor={g.shortName}>{g.shortName}</label>
            </span>
          </React.Fragment>
        ))}
      </div>
      <div className="achievements-list">
        <span></span>
        <span>
          <strong>
            <Trans>Achievement</Trans>
          </strong>
        </span>
        <span>
          <strong>
            <Trans>Achieved On</Trans>
          </strong>
        </span>
        {sortedAchievementsList?.map(achievement => {
          const playerAchievement =
            playerAchievementsLookup?.[achievement.achievementId];

          const isAchieved = playerAchievement?.scoreId != null;

          return (
            <React.Fragment key={achievement.achievementId}>
              <span>
                <AchievementCheckmark isAchieved={isAchieved} />
              </span>
              <span>{achievement.description}</span>
              {isAchieved ? (
                <Link to={`/score/${playerAchievement?.scoreId}`}>
                  {playerAchievement?.status === ScoreStatus.Legacy
                    ? 'Legacy'
                    : playerAchievement?.achievedAt && (
                        <LongDate>{playerAchievement?.achievedAt}</LongDate>
                      )}
                </Link>
              ) : (
                <span>&mdash;</span>
              )}
            </React.Fragment>
          );
        })}
      </div>
    </div>
  );
}
