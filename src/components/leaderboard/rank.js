import React from 'react';
import { SelectOrdinal } from '@lingui/macro';

export default function LeaderboardRank({ rank }) {
  if (rank == null) {
    return <>&mdash;</>;
  }

  return (
    <SelectOrdinal value={rank} one="#st" two="#nd" few="#rd" other="#th" />
  );
}
