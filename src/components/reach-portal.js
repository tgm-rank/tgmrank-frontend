import React, { useRef, useState } from 'react';
import { useIsomorphicLayoutEffect } from '@reach/utils';
import { createPortal } from 'react-dom';

/**
 * Welcome to @reach/portal!
 *
 * Creates and appends a DOM node to the end of `document.body` and renders a
 * React tree into it. Useful for rendering a natural React element hierarchy
 * with a different DOM hierarchy to prevent parent styles from clipping or
 * hiding content (for popovers, dropdowns, and modals).
 *
 * @see Docs   https://reacttraining.com/reach-ui/portal
 * @see Source https://github.com/reach/reach-ui/tree/master/packages/portal
 * @see React  https://reactjs.org/docs/portals.html
 */
/**
 * Portal
 *
 * @see Docs https://reacttraining.com/reach-ui/portal#portal
 */

var Portal = function Portal(_ref) {
  var children = _ref.children,
    _ref$type = _ref.type,
    type = _ref$type === void 0 ? 'reach-portal' : _ref$type;
  var mountNode = useRef(null);
  var portalNode = useRef(null);

  var _useState = useState(),
    forceUpdate = _useState[1];

  useIsomorphicLayoutEffect(
    function() {
      // It's possible that the content we are portal has, itself, been portaled.
      // In that case, it's important to append to the correct document element.
      if (mountNode.current) {
        var ownerDocument = mountNode.current.ownerDocument;
        portalNode.current =
          ownerDocument === null || ownerDocument === void 0
            ? void 0
            : ownerDocument.createElement(type);
        ownerDocument.body.appendChild(portalNode.current);
        forceUpdate({});
      }
      return function() {
        if (portalNode.current && portalNode.current.ownerDocument) {
          try {
            portalNode.current.ownerDocument.body.removeChild(
              portalNode.current,
            );
          } catch {}
        }
      };
    },
    [type],
  );
  return portalNode.current
    ? createPortal(children, portalNode.current)
    : React.createElement('span', {
        ref: mountNode,
      });
};

if (process.env.NODE_ENV !== 'production') {
  Portal.displayName = 'Portal';
}

export default Portal;
