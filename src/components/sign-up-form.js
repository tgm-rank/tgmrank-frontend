import React, { useState } from 'react';
import { Trans } from '@lingui/macro';
import TgmRank from '../tgmrank-api';
import { navigate } from '@reach/router';
import { useToastManager } from '../hooks/use-toast-manager';
import Link from './link';

export default function RegistrationForm() {
  const toastManager = useToastManager();

  const [state, setState] = useState({
    form: {
      username: '',
      password: '',
      email: '',
      remember: false,
      terms: false,
    },
  });

  const [isProcessing, setProcessing] = useState(false);

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      setProcessing(true);
      await TgmRank.register(state.form);

      toastManager.addSuccess('Successfully Registered! You can now log in.');
      navigate('/login');
    } catch (e) {
      toastManager.addError(e.errorString ?? e);
    } finally {
      setProcessing(false);
    }
  }

  function handleChange({ target }) {
    const value = target.type === 'checkbox' ? target.checked : target.value;
    setState(prevState => ({
      ...prevState,
      form: {
        ...prevState.form,
        [target.name]: value,
      },
    }));
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="grid-form">
        <div className="grid-col-6">
          <label htmlFor="frmEmailA">
            <Trans>Email</Trans>
          </label>
          <br />
          <input
            type="email"
            name="email"
            value={state.form.email}
            onChange={handleChange}
            id="frmEmailA"
            placeholder="you@example.com"
            disabled={isProcessing}
            required
            autoComplete="email"
          />
        </div>
        <div className="grid-col-6">
          <label htmlFor="frmUsernameA">
            <Trans>Username</Trans>
          </label>
          <br />
          <input
            type="text"
            name="username"
            value={state.form.username}
            onChange={handleChange}
            id="frmUsernameA"
            placeholder="Pick a Username"
            disabled={isProcessing}
            required
          />
        </div>
        <div className="grid-col-6">
          <label htmlFor="frmPasswordA">
            <Trans>Password</Trans>
          </label>
          <br />
          <input
            type="password"
            name="password"
            value={state.password}
            onChange={handleChange}
            id="frmPasswordA"
            placeholder="Create a password"
            required
            disabled={isProcessing}
            autoComplete="password"
          />
        </div>
        <div className="grid-col-6">
          <input
            type="checkbox"
            id="frmTerms"
            name="terms"
            value={state.form.terms}
            onChange={handleChange}
            disabled={isProcessing}
            required
          />
          <label htmlFor="frmTerms">
            <span>
              <Trans>
                I agree to the{' '}
                <Link to="/about/privacy">
                  Terms of Service and Privacy Policy
                </Link>
              </Trans>
            </span>
          </label>
        </div>
        <div className="grid-col-6">
          <button
            data-action="register"
            className="button"
            disabled={isProcessing}
          >
            {isProcessing ? (
              <Trans>Registering...</Trans>
            ) : (
              <Trans>Register</Trans>
            )}
          </button>
        </div>
      </div>
    </form>
  );
}
