import React from 'react';

import './styles.css';

import front from '../../../images/spinner/J.svg';
import back from '../../../images/spinner/L.svg';
import right from '../../../images/spinner/T.svg';
import top from '../../../images/spinner/S.svg';
import bottom from '../../../images/spinner/Z.svg';

function shuffle(a) {
  let j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}

export default function Spinner({ children }) {
  const faces = shuffle([front, back, right, top, bottom]);
  return (
    <div className="spinner__container">
      <div className="spinner">
        <div className="cube">
          <div className="cube__face cube__face--front">
            <img src={faces[0]} />
          </div>
          <div className="cube__face cube__face--back">
            <img src={faces[1]} />
          </div>
          <div className="cube__face cube__face--right">
            <img src={faces[2]} />
          </div>
          <div className="cube__face cube__face--top">
            <img src={faces[3]} />
          </div>
          <div className="cube__face cube__face--bottom">
            <img src={faces[4]} />
          </div>
        </div>
      </div>
      {children}
    </div>
  );
}
