![Plus](images/game-icons/tap.png)

# TGM Rank frontend

TGM Rank frontend is currently under development and does not connect to the API. All data is mock data.

## Installation

```
# install deps
yarn install
```
### Compiling language catalogs
```
yarn compile
```
### Development
```
# run development server
yarn start
```
### Production
```
yarn build
```

There is currently no task for building for production.

## [License](LICENSE)
